export interface Item{
  item_description?: string,
  item_note?: string,
  item_hsn_sac_code?: number,
  item_quantity?:number,
  item_unit?: string,
  item_rate?: number,
  item_taxable_value?: number,
  item_igst?:number,
  item_cgst?:number,
  item_sgst?:number,
  item_cess_rate?: number,
  item_cess_amount?: number,
  item_cess_non_advol_rate?: number,
  item_cess_non_advol_amount?: number,
  item_total?: number,

  item_tax_rate?: number,
  item_type?: string,
  item_sku_code?: string,
  item_purchase_price?: number,
  item_selling_price?: number,
  
  item_action?: string,
  item_buid?: number,
  item_discount?: number,
  item_gstinid?: number,
  item_inclusive_tax?: number,
  item_userid?: number,
  item_id?: number
}
