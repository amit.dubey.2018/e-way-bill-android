export interface Vehicle{
  ewbNo?: string,
  fromPlace?: string,
  fromState?: string,
  vehicleNo?: string,
  transMode?: string,
  transDocNo?: string,
  transDocDate?: string,
  vehicleType?: string,
  reasonRem?: string,
  reasonCode?: number
}
