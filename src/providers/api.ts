import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()

export class ApiService {

  public url = 'http://172.17.1.147:8089/api/ewb'; //Local Server URL
  // public url = 'https://sandboxgst.hostbooks.com/ewbapi/api/ewb'; //Testing Server URL
  // public url = 'https://eway.hostbooks.com/ewbapp/api/ewb'; //Production Server URL
  // public url = 'https://hbapi.hostbooks.com/ewb1.0.0/api/ewb'; //Production Server URL

  public httpOptions: any;

  constructor(public http: HttpClient) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin':'*',
        'Secret-Key':'ns4LxrMhWncg1W1ajKKJhRJFQIqzmgGEzXSGsWtVImjOww1BLztUZJyYW5e/WE6iKCZwL4wvPE/g6vxlrFvZ1sLdzfknJmQe5ByYQ2+Uge8iJ4j1LcWT6ZamL+OFquOEJvSm6vWoeLKJ9c+noizErkQwJhO2OUAj5n1F+uCSOBU='
      })
    }
  }

  post(endpoint: string, body: any) {
    console.log(this.url + '/' + endpoint);
    return this.http.post(this.url + '/' + endpoint, body, this.httpOptions);
  }

  get(endpoint: string) {
    console.log(this.url + '/' + endpoint);
    return this.http.get(this.url + '/' + endpoint, this.httpOptions);
  }

  calculateDistance(url:any){
    let header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.get(url, header);
  }

  validateEmail(email: string){
    return this.http.get(`https://api.towerdata.com/v5/ev?email=${email}&api_key=d508f004597ad477d6fb2d99bbcc2070`);
  }

  firebaseLogin(firebaseURL: any, body: any) {
    return this.http.post(firebaseURL, body);
  }
}
