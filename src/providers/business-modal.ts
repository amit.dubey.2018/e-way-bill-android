export interface BusinessModal {
    name?: string;
    email?: string;
    mobile?: string;
    tallyconnecterid?: string;
    useraccountno?: string;
    updated_at?: string;
    is_way_active?: string;
    is_gst_active?: string;
    is_tds_active?: string;
    is_acc_active?: string;
    is_pay_active?: string;
    is_tax_active?: string;
    is_lms_active?: string;
    is_partner_active?: string;
}