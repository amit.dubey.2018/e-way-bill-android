import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { MyApp } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { AppUpdate } from '@ionic-native/app-update';
import { AppAvailability } from '@ionic-native/app-availability';
import { AppVersion } from '@ionic-native/app-version';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Geolocation } from '@ionic-native/geolocation';
import { CallNumber } from '@ionic-native/call-number';
import { EmailComposer } from '@ionic-native/email-composer';
import { QRScanner } from '@ionic-native/qr-scanner';
import { Printer } from '@ionic-native/printer';
import { SocialSharing } from '@ionic-native/social-sharing';

import { FileChooser } from '@ionic-native/file-chooser';
import { FilePath } from '@ionic-native/file-path';

import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';

export const firebaseConfig = {
  apiKey: "AIzaSyBmRTkSzwCccdYFJRqPxq5eKH-Y5CDPKQs",
  authDomain: "hostbooks-c1515.firebaseapp.com",
  databaseURL: "https://hostbooks-c1515.firebaseio.com",
  projectId: "hostbooks-c1515",
  storageBucket: "hostbooks-c1515.appspot.com",
  messagingSenderId: "711599171600"
};

import { ApiService } from '../providers/api';
import { CommonService } from '../providers/common';
import { TranslateService } from '../providers/translate';
import { FirebaseService } from './../providers/firebase-service';

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    ScreenOrientation,
    AppUpdate,
    AppAvailability,
    AppVersion,
    InAppBrowser,
    Geolocation,
    CallNumber,
    EmailComposer,
    QRScanner,
    Printer,
    FileChooser,
    FilePath,
    SocialSharing,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ApiService, CommonService, TranslateService, FirebaseService
  ]
})
export class AppModule {}
