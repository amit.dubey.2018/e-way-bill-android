import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { TranslateService } from './../../providers/translate';
import { CommonService } from './../../providers/common';
import { ApiService } from './../../providers/api';
import { BillFrom } from '../../modal/bill-from-modal';

@IonicPage()
@Component({
  selector: 'page-add-bill-from',
  templateUrl: 'add-bill-from.html',
})
export class AddBillFromPage {

  billFrom: BillFrom = {};
  header_text:string = '';
  states: any = [];
  clientList: any = [];
  tempClientList: any = [];

  displayClients: boolean = false;

  mode: number = 1;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public translate: TranslateService, public common: CommonService, public api: ApiService) {
    this.header_text = this.navParams.get('header_text');
    this.mode = this.navParams.get('mode');

    if(this.common.selectedBillFrom!=null){
      let data = this.common.selectedBillFrom;
      this.billFrom.bill_from_name = data.business_name;
      this.billFrom.bill_from_gstin = data.GSTIN['gstin'];
      this.billFrom.bill_from_state = data.GSTIN['statecode'];
      this.billFrom.bill_from_state_code = data.GSTIN['statecode'];
      this.billFrom.bill_from_address_one = data.GSTIN['address1'];
      this.billFrom.bill_from_address_two = data.GSTIN['address2'];
      this.billFrom.bill_from_place = data.GSTIN['place'];
      this.billFrom.bill_from_pincode = data.GSTIN['zip'];
      console.log('Selected ', this.billFrom);
    }
    if(this.mode==2){
      let data = JSON.parse(localStorage.getItem('billDetails'));
      this.billFrom.bill_from_name = data.fromTrdName;
      this.billFrom.bill_from_gstin = data.fromGstin;
      this.billFrom.bill_from_state = data.fromStateCode;
      this.billFrom.bill_from_state_code = data.fromSupply;
      this.billFrom.bill_from_address_one = data.fromAddr1;
      this.billFrom.bill_from_address_two = data.fromAddr2;
      this.billFrom.bill_from_place = data.fromPlace;
      this.billFrom.bill_from_pincode = data.fromPincode;
      console.log('Mode ', this.billFrom);
    }

    this.clientList = JSON.parse(localStorage.getItem('clientList'));
    this.tempClientList = JSON.parse(localStorage.getItem('clientList'));
    this.getStates();
  }

  save(){
    const gstinRegxp = /^([0-9]{2}[a-zA-Z]{4}([a-zA-Z]{1}|[0-9]{1})[0-9]{4}[a-zA-Z]{1}([a-zA-Z]|[0-9]){3}){0,15}$/;
    console.log(this.billFrom);
    if (this.billFrom.bill_from_name == null || this.billFrom.bill_from_name == '') {
      this.common.displayToaster('Please select bill from name!');
      return false;
    } 
    if (this.billFrom.bill_from_gstin == null || this.billFrom.bill_from_gstin == '') {
      this.common.displayToaster('Please enter bill from GSTIN!');
      return false;
    } 
    if((this.billFrom.bill_from_gstin).toUpperCase()!='URP'){
      if ((this.billFrom.bill_from_gstin.length != 15) || (!gstinRegxp.test(this.billFrom.bill_from_gstin))) {
        this.common.displayToaster('Please enter a valid gstin number!');
        return false;
      }
    }
    if (this.billFrom.bill_from_state_code == null || this.billFrom.bill_from_state_code == '') {
      this.common.displayToaster('Please select bill from state!');
      return false;
    } 
    if (this.billFrom.bill_from_address_one == null || this.billFrom.bill_from_address_one == '') {
      this.common.displayToaster('Please enter dispatch from address 1!');
      return false;
    } 
    if (this.billFrom.bill_from_address_two == null || this.billFrom.bill_from_address_two == '') {
      this.common.displayToaster('Please enter dispatch from address 2!');
      return false;
    } 
    if (this.billFrom.bill_from_place == null || this.billFrom.bill_from_place == '') {
      this.common.displayToaster('Please enter dispatch from city!');
      return false;
    } 
    if (this.billFrom.bill_from_pincode == null || this.billFrom.bill_from_pincode == '') {
      this.common.displayToaster('Please enter dispatch from pincode!');
      return false;
    } 
    if (((this.billFrom.bill_from_pincode).toString()).length!=6) {
      this.common.displayToaster('Please enter a valid pincode!');
      return false;
    }
    if (this.billFrom.bill_from_state == null || this.billFrom.bill_from_state == '') {
      this.common.displayToaster('Please select dispatch from state!');
      return false;
    } 
    else{
      let data = this.billFrom;
      this.viewCtrl.dismiss(data);
    }
  }

  cancel(){
    let data = { };
    this.viewCtrl.dismiss(data);
  }

  getStates(){
    this.api.get('getstate').subscribe((res)=>{
      this.states = JSON.parse(atob(res['stateList']));
      (this.states).forEach((element)=>{
        if(parseInt(element.state_code)==parseInt(this.billFrom.bill_from_state)){
         this.billFrom.bill_from_state = element.state_code;
         this.billFrom.bill_from_state_code = element.state_code;
         console.log(this.billFrom.bill_from_state);
        }
      });
    }, (err)=>{
      console.log(err);
      this.common.displayToaster('Oops, Something went wrong!');
    });
  }

  searchClient(event){
    this.displayClients = true;
    this.initializeClients();
    const val = event.target.value;
    if (val && val.trim() != '') {
      this.clientList = this.clientList.filter((client) => {
        return (((client.clientname).toLowerCase()).indexOf(val.toLowerCase()) > -1);
      });
    }
    if((this.clientList).length==0){
      this.clientList.push({clientname:'No Record Found!', state_name:'', clientid: '0'});
    }
  }

  initializeClients(){
    this.clientList = this.tempClientList;
  }

  selectClient(value){
    this.displayClients = false;
    let  userid = btoa(this.common.loginData.ID);
    let  buid = btoa(this.common.selectedBusiness.buid);
    let  gstinid = btoa(this.common.selectedBusiness.GSTIN.gstinid);
    let  clientid = parseInt(value.clientid);
    let  isaction = 'V';

    this.api.get(`getclient/${userid}/${buid}/${gstinid}/${clientid}/${isaction}/`).subscribe((res)=>{
      if(res['status']==false){
      }
      else{
        let data = JSON.parse(atob(res['client']))[0];
        this.billFrom.bill_from_name = data.clientname;
        this.billFrom.bill_from_gstin = data.gstin;
        this.billFrom.bill_from_state = data.state_id;
        this.billFrom.bill_from_state_code = data.state_id;
        this.billFrom.bill_from_address_one = data.address1;
        this.billFrom.bill_from_address_two = data.address2;
        this.billFrom.bill_from_place = data.city;
        this.billFrom.bill_from_pincode = data.pincode;
        console.log(data);
      }
    }, (err)=>{
      console.log(err);
      this.common.displayToaster('Oops, Something went wrong!');
    });
  }
}
