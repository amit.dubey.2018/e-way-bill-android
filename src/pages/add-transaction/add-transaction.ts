import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { TranslateService } from './../../providers/translate';
import { CommonService } from './../../providers/common';
import { Transaction } from '../../modal/transaction-modal';

@IonicPage()
@Component({
  selector: 'page-add-transaction',
  templateUrl: 'add-transaction.html',
})
export class AddTransactionPage {

  transaction: Transaction = {};
  header_text: string = '';
  maxDate: any;
  mode: number = 1;
  transactionSubTypeList: any;
  documentTypeList: any;
  transactionTypeList: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public translate: TranslateService, public common: CommonService) {
    this.initializeType();
    let date = new Date();
    this.maxDate = this.common.disableDate(date);
    let year = date.getFullYear();
    let month = ((date.getMonth()+1)<10)?`0${(date.getMonth()+1)}`:(date.getMonth()+1);
    let day = (date.getDate()<10)?`0${date.getDate()}`:(date.getDate());
    this.transaction.document_date = `${year}-${month}-${day}`;
    this.header_text = this.navParams.get('header_text');
    this.mode = this.navParams.get('mode');
    if (this.mode == 2) {
      let data = JSON.parse(localStorage.getItem('billDetails'));
      this.transaction.transaction_supply_type = data.supplyType;
      this.transaction.transaction_sub_type = data.subSupplyType1;
      this.transaction.document_type = data.docType1;
      this.transaction.document_no = data.docNo;
      let oldDate = (data.docDate).split('/');
      this.transaction.document_date = `${oldDate[2]}-${oldDate[1]}-${oldDate[0]}`;
      this.transaction.transaction_type = parseInt(data.transactionType);
      console.log(data);
      console.log(this.transaction);
    }
  }

  save() {
    if (this.transaction.transaction_supply_type == null || this.transaction.transaction_supply_type == '') {
      this.common.displayToaster('Please select supply type!');
      return false;
    } if (this.transaction.transaction_sub_type == null || this.transaction.transaction_sub_type == '') {
      this.common.displayToaster('Please select sub type!');
      return false;
    } if (this.transaction.document_type == null || this.transaction.document_type == '') {
      this.common.displayToaster('Please select document type!');
      return false;
    } if (this.transaction.document_no == null || this.transaction.document_no == '') {
      this.common.displayToaster('Please enter document number!');
      return false;
    } if (this.transaction.document_date == null || this.transaction.document_date == '') {
      this.common.displayToaster('Please select document date!');
      return false;
    } if (this.transaction.transaction_type == null) {
      this.common.displayToaster('Please select transaction type!');
      return false;
    } else {
      let data = this.transaction;
      this.viewCtrl.dismiss(data);
    }
  }

  cancel() {
    let data = {};
    this.viewCtrl.dismiss(data);
  }

  changeType() {
    let type = this.transaction.transaction_supply_type;
    if (type == 'O') {
      this.transactionSubTypeList = [
        { value: 1, name: 'Supply' },
        { value: 3, name: 'Export' },
        { value: 9, name: 'SKD/CKD' },
        { value: 4, name: 'Job Work' },
        { value: 11, name: 'Recipient Not Known' },
        { value: 5, name: 'For Own Use' },
        { value: 12, name: 'Exhibition or Fairs' },
        { value: 10, name: 'Line Sales' },
        { value: 8, name: 'Others' },
      ];
      this.documentTypeList = [
        { value: 'INV', name: 'Sales Invoice' },
        { value: 'BIL', name: 'Bills of Supply' }
      ];
    }
    else {
      this.transactionSubTypeList = [
        { value: 1, name: 'Supply' },
        { value: 2, name: 'Import' },
        { value: 9, name: 'SKD/CKD' },
        { value: 6, name: 'Job work Returns' },
        { value: 7, name: 'Sales Return' },
        { value: 12, name: 'Exhibition or Fairs' },
        { value: 5, name: 'For Own Use' },
        { value: 8, name: 'Others' }
      ];
      this.documentTypeList = [
        { value: 'INV', name: 'Purchase Invoice' },
        { value: 'BIL', name: 'Bills of Supply' }
      ];
    }
  }

  initializeType() {

    this.transactionSubTypeList = [
      { value: 1, name: 'Supply' },
      { value: 3, name: 'Export' },
      { value: 9, name: 'SKD/CKD' },
      { value: 4, name: 'Job Work' },
      { value: 11, name: 'Recipient Not Known' },
      { value: 5, name: 'For Own Use' },
      { value: 12, name: 'Exhibition or Fairs' },
      { value: 10, name: 'Line Sales' },
      { value: 8, name: 'Others' },
    ];
    this.documentTypeList = [
      { value: 'INV', name: 'Sales Invoice' },
      { value: 'BIL', name: 'Bills of Supply' }
    ];

    this.transactionTypeList = [
      { value: 1, name: 'Regular' },
      { value: 2, name: 'Bill To - Ship To' },
      { value: 3, name: 'Bill From - Dispatch From' },
      { value: 4, name: 'Combination of 2 and 3' }
    ];
  }
}
