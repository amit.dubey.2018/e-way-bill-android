import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Printer, PrintOptions } from '@ionic-native/printer';

import { CommonService } from './../../providers/common';

declare var QRCode: any;
declare var $: any;

@IonicPage()
@Component({
  selector: 'page-print-eway-bill',
  templateUrl: 'print-eway-bill.html',
})
export class PrintEwayBillPage {

  invoiceData: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public printer: Printer, public common: CommonService) {
    this.invoiceData = this.navParams.get('invoiceData');
  }

  ionViewDidLoad(){
    let BillNo = this.invoiceData.ewayBillNo;
    let generatedby = this.invoiceData.generatedGstin;
    let docDate = this.invoiceData.ewayBillDate;

    let data = BillNo.trim() + "/" + generatedby.trim() + "/" + docDate.trim();
    let qrcode = new QRCode(document.getElementById("qrcode"), {
        width: 80,
        height: 80
    });
    qrcode.makeCode(data);
    $("#barcodes").barcode(BillNo.trim(), "code93");
  }
  returnValue(value1: any, value2: any){
    return (parseFloat(value1) * parseFloat(value2));
  }
  printEwayBill(){
    let invoice = (<HTMLBodyElement>document.getElementById('invoice-final')).innerHTML;
    this.printer.isAvailable().then((res)=>{
      let options: PrintOptions = {
        name: `HB_EWay_Bill_No_${this.invoiceData.ewayBillNo}`,
        landscape: false,
        grayscale: true
      };
      this.printer.print(invoice, options).then((res)=>{
        console.log(res);
        this.common.displayToaster('Invoice Saved Successfully!');
      }).catch((err)=>{
        console.log(err);
        this.common.displayToaster('Oops, Unable to generate invoice!');
      });
    }).catch((err)=>{
      console.log(err);
      this.common.displayToaster('Sorry, Current device is unable to generate invoice!');
    });
  }

}
