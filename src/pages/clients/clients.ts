import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { TranslateService } from './../../providers/translate';
import { CommonService } from './../../providers/common';
import { ApiService } from './../../providers/api';

@IonicPage()
@Component({
  selector: 'page-clients',
  templateUrl: 'clients.html',
})
export class ClientsPage {

  clientsList: any = [];
  tempclientsList: any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController, public translate: TranslateService, public api: ApiService, public common: CommonService) {
    this.getClients();
  }

  searchClient(event){
    this.initializeClients();
    const val = event.target.value;
    if (val && val.trim() != '') {
      this.clientsList = this.clientsList.filter((client) => {
        return ((((client.clientname).toLowerCase()).indexOf(val.toLowerCase()) > -1) || (((client.state_name).toLowerCase()).indexOf(val.toLowerCase()) > -1) || (((client.gstin).toLowerCase()).indexOf(val.toLowerCase()) > -1) || (((client.pan).toLowerCase()).indexOf(val.toLowerCase()) > -1));
      })
    }
  }

  initializeClients(){
    this.clientsList = this.tempclientsList;
  }

  addNewCustomer(){
    let data = {
      button_text: 'Add Client',
      clientData: null,
    }
    let addCustomerModal = this.modalCtrl.create('ClientDetailsPage', data);
    addCustomerModal.onDidDismiss(data => {
      console.log(data);
      if(data.role!='cancel'){
        this.getClients();
      }
    });
    addCustomerModal.present();
  }

  clientDetails(value){
    let userid = btoa(this.common.loginData.ID);
    let buid = btoa(this.common.selectedBusiness.buid);
    let gstinid = btoa(this.common.selectedBusiness.GSTIN.gstinid);
    let clientid = value.clientid;
    let isaction = 'V';
    this.api.get(`getclient/${userid}/${buid}/${gstinid}/${clientid}/${isaction}`).subscribe((res)=>{
      if(res['status']==false){
        this.common.displayToaster('Sorry, Unable to process request!');
      }
      else
      {
        let data = {
          button_text: 'Update Client',
          clientData: JSON.parse(atob(res['client']))[0],
        }
        let customerDetailsModal = this.modalCtrl.create('ClientDetailsPage', data);
        customerDetailsModal.onDidDismiss(data => {
          console.log(data);
          if(data.role!='cancel'){
            this.getClients();
          }
        });
        customerDetailsModal.present();
      }
    }, (err)=>{
      console.log(err);
      this.common.displayToaster('Oops, Something went wrong!');
    });
  }

  getClients(){
    let  userid = btoa(this.common.loginData.ID);
    let  buid = btoa(this.common.selectedBusiness.buid);
    let  gstinid = btoa(this.common.selectedBusiness.GSTIN.gstinid);
    let  clientid = 0;
    let  isaction = 'L';

    this.api.get(`getclient/${userid}/${buid}/${gstinid}/${clientid}/${isaction}/`).subscribe((res)=>{
      if(res['status']==false){
        this.clientsList = [];
      }
      else{
        this.clientsList = JSON.parse(atob(res['client']));
        this.tempclientsList = JSON.parse(atob(res['client']));
        localStorage.setItem('clientList', atob(res['client']));
      }
      console.log(this.clientsList);
    }, (err)=>{
      console.log(err);
      this.common.displayToaster('Oops, Something went wrong!');
    });
  }

}
