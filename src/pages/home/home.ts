import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController } from 'ionic-angular';
import { TranslateService } from './../../providers/translate';
import { CommonService } from './../../providers/common';
import { ApiService } from './../../providers/api';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  businessDetails: any;
  branchList: any = [];
  tempBranchList: any = [];
  businessList: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController, public alertCtrl: AlertController, public translate: TranslateService, public api: ApiService, public common: CommonService) {
  }

  ionViewDidLoad(){
    this.getBusinessDetails();
  }

  getBusinessDetails(){
    let id = btoa(this.common.loginData.ID);
    let uuid = btoa((this.common.clientData)['tallyconnecterid']);
    this.common.displayLoader('Please wait...');
    this.api.get(`get-business-list/${id}/${uuid}`).subscribe((res)=>{
      if(res['status']==false){
        this.common.displayToaster(res['message']);
      }
      else{
        localStorage.setItem('businessList', atob(res['businessList']));
        this.businessDetails = (JSON.parse(atob(res['businessList'])))[0];
        this.branchList = this.businessDetails['GSTIN'];
        this.tempBranchList = this.businessDetails['GSTIN'];
      }
      this.common.hideLoader();
    }, (err)=>{
      console.log(err);
      this.common.displayToaster('Oops, Something went wrong!');
      this.common.hideLoader();
    });
  }

  getSortedName(value){
    if(value){
      let name_array = value.split(' ');
      let firstChar: any = (name_array[0]).charAt(0);
      let secondChar: any;
      if(name_array[1]==undefined || name_array[1]==null){
        secondChar = (name_array[0]).charAt(1);
      }
      else{
        secondChar = (name_array[1]).charAt(0);
      }
      return (firstChar+''+secondChar);
    }
    else {
      return value;
    }
  }

  addBranch(business_name, buid){
    let data = {
      title_text: 'Add New Branch',
      business_name: business_name,
      buid: buid,
      mode: 1,
      action: 'G'
    };
    console.log(data);
    let addBusinessModal = this.modalCtrl.create('AddBusinessPage', data);
    addBusinessModal.onDidDismiss(data => {
      if(data.role!='cancel'){
        this.getBusinessDetails();
      }
    });
    addBusinessModal.present();
  }

  searchBranch(event){
    this.initializeBusiness();
    const val = event.target.value;
    if (val && val.trim() != '') {
      this.branchList = this.branchList.filter((branch) => {
        return (
          ((branch.gstin).toLowerCase()).indexOf(val.toLowerCase()) > -1 ||
          ((branch.branch).toLowerCase()).indexOf(val.toLowerCase()) > -1 
        );
      })
    }
  }

  initializeBusiness(){
    this.branchList = this.tempBranchList;
  }

  gotoDashboard(name, id, branch){

    let data = {
      business_name: name,
      buid: id,
      GSTIN: branch
    }
    this.common.selectedBusiness = data;

    localStorage.setItem('selectedBusiness', JSON.stringify(data));

    let userid = btoa(this.common.loginData.ID);
    let buid = btoa(data.buid);
    let gstinid = btoa(data.GSTIN.gstinid);

    this.common.displayLoader('Please wait...');
    this.api.get(`getitemmster/${userid}/${buid}/${gstinid}/0/L/`).subscribe((res)=>{
      console.log(res);
      if(res['status']==true){
        localStorage.setItem('itemList', atob(res['itemList']));
      }
      else{
        localStorage.setItem('itemList',  JSON.stringify([]));
      }
      this.api.get(`getclient/${userid}/${buid}/${gstinid}/0/L/`).subscribe((res)=>{
        console.log(res);
        if(res['status']==true){
          localStorage.setItem('clientList', atob(res['client']));
        }
        else{
          localStorage.setItem('clientList', JSON.stringify([]));
        }
        this.common.hideLoader();
        this.navCtrl.push('SidemenuPage');
      }, (err)=>{
        console.log(err);
        this.common.displayToaster('Oops, Something went wrong!');
        this.common.hideLoader();
      });
    }, (err)=>{
      console.log(err);
      this.common.displayToaster('Oops, Something went wrong!');
      this.common.hideLoader();
    });
  }

  logout(){
    const prompt = this.alertCtrl.create({
      title: 'Confirm',
      message: "Are you sure you want to logout?",
      buttons: [
        {
          text: 'No',
          cssClass: 'cancelAlertButton',
          handler: data => {
            console.log('QR & BarCode Generated!');
          }
        },
        {
          text: 'Yes',
          cssClass: 'submitAlertButton',
          handler: data => {
            localStorage.clear();
            this.common.displayToaster('Logged Out Successfully!');
            localStorage.setItem('language', this.translate.selectedlanguage);
            this.navCtrl.setRoot('LoginPage');
          }
        }
      ]
    });
    prompt.present();
  }

}
