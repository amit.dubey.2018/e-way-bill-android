import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GeneratedBillDetailsPage } from './generated-bill-details';

@NgModule({
  declarations: [
    GeneratedBillDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(GeneratedBillDetailsPage),
  ],
})
export class GeneratedBillDetailsPageModule {}
