import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController } from 'ionic-angular';
import { TranslateService } from './../../providers/translate';
import { CommonService } from './../../providers/common';
import { ApiService } from './../../providers/api';

import { Transaction } from '../../modal/transaction-modal';
import { BillFrom } from '../../modal/bill-from-modal';
import { BillTo } from '../../modal/bill-to-modal';
import { Item } from '../../modal/item-modal';
import { Transport } from '../../modal/transport-modal';

@IonicPage()
@Component({
  selector: 'page-add-bill',
  templateUrl: 'add-bill.html',
})
export class AddBillPage {

  transaction: Transaction = {};
  billFrom: BillFrom = {};
  billTo: BillTo = {};
  items: Item = {};
  transport: Transport = {};

  transactionDetails: boolean = false;
  billFromDetails: boolean = true;
  billToDetails: boolean = true;
  itemsDetails: boolean = true;
  transportDetails: boolean = true;

  itemsList = [];
  states = [];

  fromStateCode: any = 0;
  toStateCode: any = 0;

  startAddress: any = '';
  endAddress: any = '';

  transaction_supply_type: any = '';
  transaction_sub_type: any = '';
  document_type: any = '';
  transaction_type: any = '';

  billFromStateName: any = '';
  dispatchFromStateName: any = '';
  billToStateName: any = '';
  dispatchToStateName: any = '';

  transportation_mode: any = '';
  vehicle_type: any = '';
  vehicle_number: any = '';

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController, public alertCtrl: AlertController, public translate: TranslateService, public api: ApiService, public common: CommonService) {
    this.getStates();
  }

  showTransactionDetails() {
    this.transactionDetails = !this.transactionDetails;
  }

  showBillFromDetails() {
    this.billFromDetails = !this.billFromDetails;
  }

  showBillToDetails() {
    this.billToDetails = !this.billToDetails;
  }

  showItemDetails() {
    this.itemsDetails = !this.itemsDetails;
  }

  showTransportDetails() {
    this.transportDetails = !this.transportDetails;
  }

  addTransaction() {
    let data = {
      header_text: 'Add Transaction',
      mode: 1
    };
    let addTransactionModal = this.modalCtrl.create('AddTransactionPage', data);
    addTransactionModal.onDidDismiss(res => {
      this.transaction = res;
      if (res['transaction_supply_type'] == 'O') {
        this.common.selectedBillFrom = this.common.selectedBusiness;
        this.common.selectedBillTo = null;
        this.transaction_supply_type = 'Outward-Supply';
      }
      else if (res['transaction_supply_type'] == 'I') {
        this.common.selectedBillTo = this.common.selectedBusiness;
        this.common.selectedBillFrom = null;
        this.transaction_supply_type = 'Inward-Supply';
      }
      this.transactionSubType(this.transaction.transaction_sub_type);
      this.documentType(this.transaction.document_type);
      this.transactionType(this.transaction.transaction_type);
    });
    addTransactionModal.present();
  }

  addBillFrom() {
    let data = {
      header_text: 'Add Bill From',
      mode: 1
    };
    let addTransactionModal = this.modalCtrl.create('AddBillFromPage', data);
    addTransactionModal.onDidDismiss(res => {
      this.billFrom = res;
      this.fromStateCode = res.bill_from_state;
      this.startAddress = `${res.bill_from_place}, ${res.bill_from_pincode}`;
      (this.states).forEach((element) => {
        if (element.state_code == res.bill_from_state_code) {
          this.billFromStateName = element.state_name;
        }
        if (element.state_code == res.bill_from_state) {
          this.dispatchFromStateName = element.state_name;
        }
      });
    });
    addTransactionModal.present();
  }

  addBillTo() {
    let data = {
      header_text: 'Add Bill To',
      mode: 1
    };
    let addTransactionModal = this.modalCtrl.create('AddBillToPage', data);
    addTransactionModal.onDidDismiss(res => {
      this.billTo = res;
      this.toStateCode = res.bill_to_state;
      this.endAddress = `${res.bill_to_place}, ${res.bill_to_pincode}`;
      (this.states).forEach((element) => {
        if (element.state_code == res.bill_to_state_code) {
          this.billToStateName = element.state_name;
        }
        if (element.state_code == res.bill_to_state) {
          this.dispatchToStateName = element.state_name;
        }
      });
    });
    addTransactionModal.present();
  }

  addItem() {
    if (this.fromStateCode == 0 || this.toStateCode == 0) {
      this.common.displayToaster('Please select bill from & bill to state');
      return false;
    } else {
      let data = {
        header_text: 'Add Item',
        mode: 1,
        fromStateCode: this.fromStateCode,
        toStateCode: this.toStateCode
      };
      let addTransactionModal = this.modalCtrl.create('AddItemPage', data);
      addTransactionModal.onDidDismiss(res => {
        if (res.item_description != '') {
          let foundMatch = 0;
          (this.itemsList).forEach(element => {
            if (element.item_id == res.item_id) {
              foundMatch = 1;
            }
          });
          if (foundMatch == 0) {
            this.itemsList.push(res);
          } else {
            this.common.displayToaster('Sorry, You can not add same item more than one!');
          }
        }
        console.log(this.itemsList);
      });
      addTransactionModal.present();
    }
  }

  updateItem(item) {
    let data = {
      header_text: 'Update Item',
      mode: 2,
      itemData: item,
      fromStateCode: this.fromStateCode,
      toStateCode: this.toStateCode
    };
    console.log(item);
    let AddItemPageModal = this.modalCtrl.create('AddItemPage', data);
    AddItemPageModal.onDidDismiss(res => {
      (this.itemsList).forEach(element => {
        if (element.item_id == res.item_id) {
          element.item_id = res.item_id;
          element.itemDescription = res.itemDescription;
          element.description = res.description;
          element.hsnSac = res.hsnSac;
          element.qty = parseFloat(res.qty);
          element.unitofmeasurement = res.unitofmeasurement;
          element.rate = parseFloat(res.rate);
          element.taxablevalue1 = parseFloat(res.taxablevalue1);
          element.taxablerate = parseFloat(res.taxablerate);
          element.cessrate = parseFloat(res.cessrate);
          element.cgst = parseFloat(res.cgst);
          element.cgstRate = parseFloat(res.cgstRate);
          element.sgst = parseFloat(res.sgst);
          element.sgstRate = parseFloat(res.sgstRate);
          element.igst = parseFloat(res.igst);
          element.igstRate = (res.igstRate);
          element.cessAmt = parseFloat(res.cessAmt);
          element.cessAdvol = parseFloat(res.cessAdvol);
          element.total = parseFloat(res.total);
        }
      });
    });
    AddItemPageModal.present();
  }

  deleteItem(item) {
    const alert = this.alertCtrl.create({
      title: 'Confirm',
      message: 'Your item will be removed from list.',
      buttons: [
        {
          text: 'No',
          cssClass: 'cancelAlertButton',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          cssClass: 'submitAlertButton',
          handler: data => {
            (this.itemsList).forEach(element => {
              let index = this.itemsList.indexOf(item);
              if (index > -1) {
                this.itemsList.splice(index, 1);
              }
            });
          }
        }
      ]
    });
    alert.present();
  }

  addTransport() {
    if (this.fromStateCode == 0 || this.toStateCode == 0) {
      this.common.displayToaster('Please select bill from & bill to state');
      return false;
    } else {
      let data = {
        header_text: 'Add Transport',
        startAddress: this.startAddress,
        endAddress: this.endAddress,
        mode: 1
      };
      let addTransportModal = this.modalCtrl.create('AddTransportPage', data);
      addTransportModal.onDidDismiss(res => {
        this.transport = res;
        if (parseInt(this.transport.transportation_mode) == 1) {
          this.transportation_mode = 'By Road';
          if (this.transport.vehicle_type == 'R') {
            this.vehicle_type = 'Regular';
          } else {
            this.vehicle_type = 'Over Dimensional Cargo';
          }
          this.vehicle_number = this.transport.vehicle_number;
        }
        if (parseInt(this.transport.transportation_mode) == 2) {
          this.transportation_mode = 'By Rail';
          this.vehicle_type = 'Rail';
          this.vehicle_number = 'Not Available';
        }
        if (parseInt(this.transport.transportation_mode) == 3) {
          this.transportation_mode = 'By Air';
          this.vehicle_type = 'Air';
          this.vehicle_number = 'Not Available';
        }
        if (parseInt(this.transport.transportation_mode) == 4) {
          this.transportation_mode = 'By Ship';
          this.vehicle_type = 'Ship';
          this.vehicle_number = 'Not Available';
        }
      });
      addTransportModal.present();
    }
  }

  getStates() {
    this.api.get('getstate').subscribe((res) => {
      this.states = JSON.parse(atob(res['stateList']));
    }, (err) => {
      console.log(err);
      this.common.displayToaster('Oops, Something went wrong!');
    });
  }

  transactionSubType(value: any) {
    let transactionSubTypeList = [
      { value: 2, name: 'Import' },
      { value: 12, name: 'Exhibition or Fairs' },
      { value: 3, name: 'Export' },
      { value: 5, name: 'For Own Use' },
      { value: 4, name: 'Job Work' },
      { value: 10, name: 'Line Sales' },
      { value: 8, name: 'Others' },
      { value: 11, name: 'Recepient Not Known' },
      { value: 9, name: 'SKD/CKD' },
      { value: 1, name: 'Supply' },
      { value: 6, name: 'Job work Returns' },
      { value: 7, name: 'Sales Return' },
      { value: 8, name: 'Others' },
      { value: 1, name: 'Supply' }
    ];
    transactionSubTypeList.forEach((element) => {
      if (element.value == value) {
        this.transaction_sub_type = element.name;
      }
    });
  }

  documentType(value: any) {
    let documentTypeList = [
      { value: 'BOE', name: 'Advance Receipt' },
      { value: 'BIL', name: 'Bills of Supply' },
      { value: 'CNT', name: 'Credit Note' },
      { value: 'CHL', name: 'Delivery Challan' },
      { value: 'OTH', name: 'Other' },
      { value: 'INV', name: 'Sales Invoice' },
      { value: 'INV', name: 'Purchase Invoice' }
    ];
    documentTypeList.forEach((element) => {
      if (element.value == value) {
        this.document_type = element.name;
      }
    });
  }

  transactionType(value: any) {
    let transactionTypeList = [
      { value: 1, name: 'Regular' },
      { value: 2, name: 'Bill To - Ship To' },
      { value: 3, name: 'Bill From - Dispatch From' },
      { value: 4, name: 'Combination of 2 and 3' }
    ];
    transactionTypeList.forEach((element) => {
      if (element.value == value) {
        this.transaction_type = element.name;
      }
    });
  }

  save() {
    let docDate = new Date(this.transaction.document_date);
    let transDocDate = new Date(this.transport.transportation_date);

    console.log(this.transaction);
    console.log(this.billFrom);
    console.log(this.billTo);
    console.log(this.transport);
    console.log(this.itemsList);

    if (this.transaction.transaction_supply_type == '' || this.billFrom.bill_from_name == '' || this.billTo.bill_to_name == '' || this.itemsList.length == 0 || this.transport.transportion_name == '') {
      this.common.displayToaster('Please fill all form correctly!');
      return false;
    }
    if (docDate.getTime() > transDocDate.getTime()) {
      this.common.displayToaster('Transport date must be less than doc date!');
      return false;
    }

    let totalCgst = 0;
    let totalSgst = 0;
    let totalIgst = 0;
    let total_cess_value = 0;
    let total_taxable_value = 0;
    let total_invoice_value = 0;
    let non_add_value = 0;
    let otherValue = 0;
    (this.itemsList).forEach((element) => {
      totalCgst = totalCgst + element.cgst;
      totalSgst = totalSgst + element.sgst;
      totalIgst = totalIgst + element.igst;
      total_cess_value = total_cess_value + element.cessAmt;
      non_add_value = non_add_value + element.cessnonadvolAmt;
      total_taxable_value = total_taxable_value + element.taxablevalue1;
      total_invoice_value = total_invoice_value + element.total;
    });

    let fromState = null;
    let toState = null;
    this.states.forEach((element) => {
      if (element.state_code == this.billFrom.bill_from_state) {
        fromState = element.state_name;
      }
      if (element.state_code == this.billTo.bill_to_state) {
        toState = element.state_name;
      }
    });

    let data = {
      userid: this.common.loginData.ID,
      buid: this.common.selectedBusiness.buid,
      gstinid: this.common.selectedBusiness.GSTIN.gstinid,
    
      supplyType: this.transaction.transaction_supply_type,
      supplyTypeDesc: null,
      othersubtype: null,
      subSupplyType1: this.transaction.transaction_sub_type,
    
      docType: this.transaction.document_type,
      docType1: null,
      docNo: this.transaction.document_no,
      docDate: this.common.formatDate(this.transaction.document_date),
    
      fromGstin: this.billFrom.bill_from_gstin,
      fromSupply: this.billFrom.bill_from_state_code,
      fromSupplyDesc: null,
      fromTrdName: this.billFrom.bill_from_name,
      fromAddr1: this.billFrom.bill_from_address_one,
      fromAddr2: this.billFrom.bill_from_address_two,
      fromPlace: this.billFrom.bill_from_place,
      fromPincode: this.billFrom.bill_from_pincode,
      fromStateCode: this.billFrom.bill_from_state,
    
      toGstin: this.billTo.bill_to_gstin,
      toSupply: this.billTo.bill_to_state_code,
      toSupplyDesc: null,
      toTrdName: this.billTo.bill_to_name,
      toAddr1: this.billTo.bill_to_address_one,
      toAddr2: this.billTo.bill_to_address_two,
      toPlace: this.billTo.bill_to_place,
      toPincode: this.billTo.bill_to_pincode,
      toStateCode: this.billTo.bill_to_state,
    
      totalTaxableValue: total_invoice_value,
      totalValue: total_taxable_value,
      cgstValue: totalCgst,
      sgstValue: totalSgst,
      igstValue: totalIgst,
      cessValue: total_cess_value,
      nonadvolcessValue: non_add_value,
      otherValue: otherValue,
    
      transporterId: this.transport.gstin,
      transporterName: this.transport.transportion_name,
      transDocNo: this.transport.transport_doc_no,
      transMode: this.transport.transportation_mode,
      transModeDesc: null,
      transDistance: this.transport.approx_distance,
      transDocDate: this.common.formatDate(this.transport.transportation_date),
      transactionType: this.transaction.transaction_type,
    
      vehicleNo: this.transport.vehicle_number,
      vehicleType: this.transport.vehicle_type,
      isaction: 'I',
    
      ewayBillNo: null,
      ewayBillDate: null,
    
      validUpto: null,
      fromStateName: fromState,
      toStateName: toState,
      userGstin: this.common.selectedBusiness.GSTIN.gstin,
      generatedName: null,
      generatedGstin: null,
      isstatus: null,
      rejectStatus: null,
    
      itemList: this.itemsList,
    }
    
    console.log(data);
    this.common.displayLoader('Please wait...');
    this.api.post('add-ewb-bill', data).subscribe((res) => {
      console.log(res);
      if (res['status'] == false) {
        this.common.displayToaster(res['message']);
      }
      else {
        this.common.displayToaster('Bill Saved Successfully!');
        this.navCtrl.pop();
      }
      this.common.hideLoader();
    }, (err) => {
      console.log(err);
      this.common.displayToaster('Oops, Something went wrong!');
      this.common.hideLoader();
    });
  }

}
