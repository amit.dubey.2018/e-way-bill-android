import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { TranslateService } from './../../providers/translate';
import { CommonService } from './../../providers/common';
import { ApiService } from './../../providers/api';

@IonicPage()
@Component({
  selector: 'page-items',
  templateUrl: 'items.html',
})
export class ItemsPage {

  itemsList: any = [];
  tempItemList: any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController, public translate: TranslateService, public api: ApiService, public common: CommonService) {
    this.getItems();
  }

  searchItem(event){
    this.initializeItems();
    const val = event.target.value;
    if (val && val.trim() != '') {
      this.itemsList = this.itemsList.filter((item) => {
        console.log(item);
        return ((((item.item_description).toLowerCase()).indexOf(val.toLowerCase()) > -1) || (((item.hsn_sac_code).toLowerCase()).indexOf(val.toLowerCase()) > -1) || (((item.item_sku_code).toLowerCase()).indexOf(val.toLowerCase()) > -1));
      })
    }
  }

  initializeItems(){
    this.itemsList = this.tempItemList;
  }

  addNewItem(){
    let data = {
      button_text: 'Add Item',
      itemData: null
    }
    let addItemModal = this.modalCtrl.create('ItemDetailsPage', data);
    addItemModal.onDidDismiss(data => {
      console.log(data);
      if(data.role!='cancel'){
        this.getItems();
      }
    });
    addItemModal.present();
  }

  itemDetails(value){
    let userid = btoa(this.common.loginData.ID);
    let buid = btoa(this.common.selectedBusiness.buid);
    let gstinid = btoa(this.common.selectedBusiness.GSTIN.gstinid);
    let itemid = value.itemid;
    let isaction = 'V';
    this.api.get(`getitemmster/${userid}/${buid}/${gstinid}/${itemid}/${isaction}`).subscribe((res)=>{
      if(res['status']==false){
        this.common.displayToaster('Sorry, Unable to process request!');
      }
      else
      {
        let data = {
          button_text: 'Update Item',
          itemData: JSON.parse(atob(res['itemList']))[0]
        }
        let itemDetailsModal = this.modalCtrl.create('ItemDetailsPage', data);
        itemDetailsModal.onDidDismiss(data => {
          console.log(data);
          if(data.role!='cancel'){
            this.getItems();
          }
        });
        itemDetailsModal.present();
      }
    }, (err)=>{
      console.log(err);
      this.common.displayToaster('Oops, Something went wrong!');
    });

  }

  getItems(){
    let userid = btoa(this.common.loginData.ID);
    let buid = btoa(this.common.selectedBusiness.buid);
    let gstinid = btoa(this.common.selectedBusiness.GSTIN.gstinid);
    let itemid = 0;
    let isaction = 'L';
    this.api.get(`getitemmster/${userid}/${buid}/${gstinid}/${itemid}/${isaction}`).subscribe((res)=>{
      console.log(res);
      if(res['status']==false){
        this.itemsList = [];
      }
      else{
        this.itemsList = JSON.parse(atob(res['itemList']));
        this.tempItemList = JSON.parse(atob(res['itemList']));
        localStorage.setItem('itemList', atob(res['itemList']));
      }
      console.log(this.itemsList);
    }, (err)=>{
      console.log(err);
      this.common.displayToaster('Oops, Something went wrong!');
    });
  }

  sortPurchasePrice(price){
    return (parseFloat(price)).toFixed(2);
  }

  sortSellingPrice(price){
    return (parseFloat(price)).toFixed(2);
  }

  sortCessPrice(price){
    return (parseFloat(price)).toFixed(2);
  }
}
