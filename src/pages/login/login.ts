import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

import { AppAvailability } from '@ionic-native/app-availability';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Platform } from 'ionic-angular';

import { TranslateService } from './../../providers/translate';
import { CommonService } from './../../providers/common';
import { ApiService } from './../../providers/api';

import { FirebaseService } from '../../providers/firebase-service';

import * as launcher from '../../assets/js/start-app.js';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  email: string = '';

  constructor(public appAvailability: AppAvailability, private iab: InAppBrowser, public platform: Platform, public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public translate: TranslateService, public common: CommonService, public api: ApiService, public firebaseService: FirebaseService) {
  }

  ionViewDidEnter() {
    let user = localStorage.getItem('loginData');
    if (user) {
      this.navCtrl.setRoot('HomePage');
    }
  }

  login() {
    const emailRegexp = /^[a-zA-Z0-9._%+-]+@[a-zA-Z]+\.[a-zA-Z]{2,4}$/;
    if (!emailRegexp.test(this.email)) {
      this.common.displayToaster('Please enter an valid email address!');
    }
    else {
      this.common.displayLoader('Please wait....');
      this.firebaseService.findBusiness(this.email).then((res) => {
        let value = res.val();
        let key = null;
        res.forEach((element) => {
          key = element.key;
        });
        console.log(value, key);
        if (key != null && value != null) {
          this.updateBusiness(value[key], key);
        } else {
          this.common.displayToaster(`You have not activated any business yet!`);
          this.common.hideLoader();
          this.activateBusiness();
        }
      }).catch((err) => {
        console.log(err);
        this.common.displayToaster('Unable to find business!');
        this.common.hideLoader();
      });
    }
  }

  addBusiness() {
    this.common.displayLoader('Please wait...');
    const business = {
      name: 'Amit Dubey',
      email: this.email,
      mobile: '7610002325',
      tallyconnecterid: '8d9602f4-0f2f-11e9-b5f8-e0071b1b10d4',
      useraccountno: 'EZNZAX32730',
      updated_at: (new Date()).toISOString(),
      is_way_active: 'active'
    };
    this.firebaseService.addBusiness(business).then((res) => {
      console.log('Business added successfully!');
      this.finalLogin(business);
    }).catch((err) => {
      console.log(err);
      this.common.displayToaster('Unable to add business!');
      this.common.hideLoader();
    });
  }

  updateBusiness(data, key) {
    const business = {
      name: data['name'],
      email: data['email'],
      mobile: data['mobile'],
      tallyconnecterid: data['tallyconnecterid'],
      useraccountno: data['useraccountno'],
      updated_at: (new Date()).toISOString(),
      is_way_active: 'active'
    };
    this.firebaseService.updateBusiness(business, key).then((res) => {
      console.log('Business updated successfully!');
      this.finalLogin(data);
    }).catch((err) => {
      console.log(err);
      this.common.displayToaster('Unable to update business!');
      this.common.hideLoader();
    });
  }

  finalLogin(data) {
    this.api.post('login', { tallyconnecterid: data['tallyconnecterid'], useraccountno: data['useraccountno'] }).subscribe((res) => {
      this.common.hideLoader();
      if (res['status'] == false) {
        this.common.displayToaster(res['message']);
      }
      else {
        this.common.clientData = data;
        localStorage.setItem('clientData', JSON.stringify(data));
        this.common.loginData = JSON.parse(atob(res['result']));
        localStorage.setItem('loginData', atob(res['result']));
        this.common.displayToaster('Logged In Successfully!');
        this.navCtrl.setRoot('HomePage');
      }
    }, (err) => {
      console.log(err);
      this.common.displayToaster('Unable to login!');
      this.common.hideLoader();
    });
  }

  activateBusiness() {
    const prompt = this.alertCtrl.create({
      title: 'Activate Business',
      message: "You have to install HostBooks App.",
      buttons: [
        {
          text: 'Cancel',
          cssClass: 'cancelAlertButton',
          handler: data => {
            console.log('Process cancelled!');
          }
        },
        {
          text: 'Install',
          cssClass: 'submitAlertButton',
          handler: data => {
            this.appAvailability.check('com.application.zomato')
              .then((yes: boolean) => {
                console.log('App is available');
                launcher.packageLaunch('com.application.zomato');
              }, (no: boolean) => {
                console.log('App is not available');
                if (this.platform.is('ios')) {
                  this.iab.create('https://play.google.com/store/apps/details?id=hostbooks.com.eway.bill', '_blank', 'location=no');
                } else {
                  this.iab.create('https://play.google.com/store/apps/details?id=hostbooks.com.eway.bill', '_blank', 'location=no');
                }
              }
            );
          }
        }
      ]
    });
    prompt.present();
  }
}
