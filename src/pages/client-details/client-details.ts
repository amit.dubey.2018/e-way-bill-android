import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { TranslateService } from './../../providers/translate';
import { CommonService } from './../../providers/common';
import { ApiService } from './../../providers/api';
import { Customer } from '../../modal/customer-modal';

declare var google;

@IonicPage()
@Component({
  selector: 'page-client-details',
  templateUrl: 'client-details.html',
})
export class ClientDetailsPage {

  @ViewChild('map') mapElement: ElementRef;
  map: any;

  customer: Customer = {};
  button_text: string;
  clientData: any = {};
  countries: any = [];
  states: any = [];
  gstinCode: any;

  address: any;
  options: any;

  invalidGstin:  boolean = false;
  invalidGstinMessage: any;
  invalidPan:  boolean = false;
  invalidPanMessage: any;
  invalidMobile: boolean = false;
  invalidMobileMessage: any;
  invalidLandline: boolean = false;
  invalidLandlineMessage: any;
  invalidPincode: boolean = false;
  invalidPincodeMessage: any;

  constructor(public geolocation: Geolocation, public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public translate: TranslateService, public api: ApiService, public common: CommonService) {
    this.customer.customer_country = '99';
    this.button_text = this.navParams.get('button_text');
    this.clientData = this.navParams.get('clientData');
    if(this.clientData != null){
      this.customer.customer_id = parseInt(this.clientData.clientid);
      this.customer.customer_name = this.clientData.clientname;
      this.customer.customer_nickname = this.clientData.nickname;
      this.customer.customer_gstin = this.clientData.gstin;
      this.customer.customer_contact_person = this.clientData.contact_person;
      this.customer.customer_mobile = this.clientData.mobile_no;
      this.customer.customer_pan = this.clientData.pan;
      this.customer.customer_email = this.clientData.email_id;
      this.customer.customer_landline = this.clientData.phone_no;
      this.customer.customer_country = this.clientData.country_id;
      this.customer.customer_state = this.clientData.state_id;
      this.customer.customer_city = this.clientData.city;
      this.customer.customer_address_one = this.clientData.address1;
      this.customer.customer_address_two = this.clientData.address2;
      this.customer.customer_pincode = this.clientData.pincode;
      this.gstinCode = this.customer.customer_state;
    }
    console.log(this.clientData);
    this.getCountries();
    this.getStates();
  }

  ionViewDidLoad() {
    this.address = (<HTMLInputElement>document.getElementById("customer_address_one").getElementsByTagName('input')[0]);
    this.options = {
      types: [],
      componentRestrictions: {country: 'in'}
    };
    var componentForm = {
      sublocality_level_1: 'long_name',
      sublocality_level_2: 'long_name',
      locality: 'long_name',
      administrative_area_level_1: 'long_name',
      administrative_area_level_2: 'long_name',
      postal_code: 'long_name'
    };
    let autocomplete = new google.maps.places.Autocomplete(this.address, this.options);
    autocomplete.addListener('place_changed', ()=>{
      let place = autocomplete.getPlace();
      console.log(place);
      this.customer.customer_address_one= `${place.formatted_address}`;
      this.customer.customer_address_two = `${place.name}`;
      for (let i = 0; i < place.address_components.length; i++){
        let addressType = place.address_components[i].types[0];
        if (componentForm[addressType]) {
          let val = place.address_components[i][componentForm[addressType]];
          if ((place.address_components[i].types[0] == 'locality') || (place.address_components[i].types[0] == 'administrative_area_level_2')){
            this.customer.customer_city = val;
          }
          if(place.address_components[i].types[0]=='postal_code'){
            this.customer.customer_pincode = val;
          }

        }
      }
    });
  }

  filterState(event){
    let gstin = event.target.value;
    const gstinRegxp = /^([0-9]{2}[a-zA-Z]{4}([a-zA-Z]{1}|[0-9]{1})[0-9]{4}[a-zA-Z]{1}([a-zA-Z]|[0-9]){3}){0,15}$/;
    if(gstin.length>=2){
      let id = gstin.substring(0, 2);
      (this.states).forEach((element)=>{
        if(element.state_code==id){
          this.customer.customer_state = element.state_code;
          this.gstinCode = id;
        }
      });
    }
    if((this.customer.customer_gstin).toUpperCase()!='URP'){
      if((this.customer.customer_gstin.length != 15 || !gstinRegxp.test(this.customer.customer_gstin))){
        this.invalidGstin = true;
        this.invalidGstinMessage = 'Invalid GSTIN Number';
      }
      else{
        this.invalidGstin = false;
      }
    }
    else {
      this.invalidGstin = false;
    }
  }

  validatePan(){
    const panRegxp = /^[a-zA-Z]{3}[abcfghljptkABCFGHLJPTK]{1}[a-zA-Z]{1}[0-9]{4}[a-zA-Z]{1}$/;
    if((this.customer.customer_pan.length != 10 || !panRegxp.test(this.customer.customer_pan))){
      this.invalidPan = true;
      this.invalidPanMessage = 'Invalid Pan Number';
    }
    else{
      this.invalidPan = false;
    }
  }

  validateMobile(){
    if((((this.customer.customer_mobile).toString()).length>10)||(((this.customer.customer_mobile).toString()).length<10)){
      this.invalidMobile = true;
      this.invalidMobileMessage = 'Invalid Phone Number';
    }
    else{
      this.invalidMobile = false;
    }
  }

  validatePincode(){
    if((((this.customer.customer_pincode).toString()).length>6)||(((this.customer.customer_pincode).toString()).length<6)){
      this.invalidPincode = true;
      this.invalidPincodeMessage = 'Invalid Pin Code';
    }
    else{
      this.invalidPincode = false;
    }
  }

  save(){
    const emailRegexp = /^[a-zA-Z0-9._%+-]+@[a-zA-Z]+\.[a-zA-Z]{2,4}$/;
    const gstinRegxp = /^([0-9]{2}[a-zA-Z]{4}([a-zA-Z]{1}|[0-9]{1})[0-9]{4}[a-zA-Z]{1}([a-zA-Z]|[0-9]){3}){0,15}$/;
    const panRegxp = /^[a-zA-Z]{3}[abcfghljptkABCFGHLJPTK]{1}[a-zA-Z]{1}[0-9]{4}[a-zA-Z]{1}$/;
    console.log(this.customer);
    if (this.customer.customer_name == null || this.customer.customer_name == '') {
      this.common.displayToaster('Please enter customer name!');
      return false;
    }
    if (this.customer.customer_nickname == null || this.customer.customer_nickname == '') {
      this.common.displayToaster('Please enter customer nickname!');
      return false;
    }
    if (this.customer.customer_gstin == null || this.customer.customer_gstin == '') {
      this.common.displayToaster('Please enter customer GSTIN!');
      return false;
    }
    if((this.customer.customer_gstin).toUpperCase()!='URP'){
      if ((this.customer.customer_gstin.length != 15) || (!gstinRegxp.test(this.customer.customer_gstin))) {
        this.common.displayToaster('Please enter a valid gstin number!');
        return false;
      }
    }
    if (this.customer.customer_contact_person == null || this.customer.customer_contact_person == '') {
      this.common.displayToaster('Please enter contact person!');
      return false;
    }
    if (this.customer.customer_mobile == null || (this.customer.customer_mobile).toString() == '') {
      this.common.displayToaster('Please enter customer mobile number!');
      return false;
    }
    if (isNaN(this.customer.customer_mobile) || ((((this.customer.customer_mobile).toString())).length!=10)) {
      this.common.displayToaster('Please enter an valid mobile number!');
      return false;
    }
    if (this.customer.customer_pan == null || this.customer.customer_pan== '') {
      this.common.displayToaster('Please enter customer pan number!');
      return false;
    }
    if ((this.customer.customer_pan.length != 10 || !panRegxp.test(this.customer.customer_pan))) {
      this.common.displayToaster('Please enter a valid pan number!');
      return false;
    }
    if (this.customer.customer_email == null || this.customer.customer_email== '') {
      this.common.displayToaster('Please enter customer email address!');
      return false;
    }
    if ((this.customer.customer_email.length <= 5 || !emailRegexp.test(this.customer.customer_email))) {
      this.common.displayToaster('Please enter a valid email address!');
      return false;
    }
    if (this.customer.customer_state == null || this.customer.customer_state == '') {
      this.common.displayToaster('Please enter customer state!');
      return false;
    }
    if (this.customer.customer_address_one == null || this.customer.customer_address_one == '') {
      this.common.displayToaster('Please enter customer address 1!');
      return false;
    }
    if (this.customer.customer_address_two == null || this.customer.customer_address_two == '') {
      this.common.displayToaster('Please enter customer address 2!');
      return false;
    }
    if (this.customer.customer_city == null || this.customer.customer_city == '') {
      this.common.displayToaster('Please enter customer city!');
      return false;
    }
    if (this.customer.customer_pincode == null || (this.customer.customer_pincode).toString()== '') {
      this.common.displayToaster('Please enter customer pincode!');
      return false;
    }
    if (isNaN(this.customer.customer_pincode) || ((((this.customer.customer_pincode).toString())).length!=6)) {
      this.common.displayToaster('Please enter a valid pincode!');
      return false;
    }
    if((this.customer.customer_gstin).toUpperCase()!='URP'){
      if (this.gstinCode!=this.customer.customer_state) {
        this.common.displayToaster('Client GSTINID does not match with state name!');
        return false;
      }
      else{
        let clientid: any = 0;
        let action: any = 'I';
        let message = 'Client added successfully!';

        if(this.customer.customer_id){
          clientid = this.customer.customer_id;
          action = 'U';
          message = 'Client updated successfully!';
        }

        let data = {
          userid: this.common.loginData.ID,
          clientid: clientid,
          gstinid: this.common.selectedBusiness.GSTIN.gstinid,
          buid: this.common.selectedBusiness.buid,
          clientname: this.customer.customer_name,
          nickname: (this.customer.customer_nickname!=null)?(this.customer.customer_nickname):this.customer.customer_name,
          gstin: (this.customer.customer_gstin).toUpperCase(),
          country_id: this.customer.customer_country,
          state_id: this.customer.customer_state,
          contact_person: this.customer.customer_contact_person,
          mobile_no: this.customer.customer_mobile,
          pan: (this.customer.customer_pan).toUpperCase(),
          address1: this.customer.customer_address_one,
          address2: this.customer.customer_address_two,
          pincode: this.customer.customer_pincode,
          city: this.customer.customer_city,
          email_id: (this.customer.customer_email).toLowerCase(),
          phone_no: (this.customer.customer_landline!=null)?(this.customer.customer_landline):this.customer.customer_mobile,
          isaction: action,
          state_name: this.customer.customer_state,
        };

        console.log(data);
        this.common.displayLoader('Please wait...');
        this.api.post('addclient', data).subscribe((res)=>{
          this.common.hideLoader();
         if((res['status']==false)&&(parseInt(res['message'])==1))
          {
            this.common.displayToaster('Sorry, Client already present in your list!');
          }
          else if(res['status']==false)
          {
            this.common.displayToaster(res['message']);
          }
          else
          {
            this.common.displayToaster(message);
            this.viewCtrl.dismiss(res);
          }
        }, (err)=>{
          console.log(err);
          this.common.displayToaster('Oops, Something went wrong!');
          this.common.hideLoader();
        });
      }
    }
    else{
      let clientid: any = 0;
      let action: any = 'I';
      let message = 'Client added successfully!';

      if(this.customer.customer_id){
        clientid = this.customer.customer_id;
        action = 'U';
        message = 'Client updated successfully!';
      }

      let data = {
        userid: this.common.loginData.ID,
        clientid: clientid,
        gstinid: this.common.selectedBusiness.GSTIN.gstinid,
        buid: this.common.selectedBusiness.buid,
        clientname: this.customer.customer_name,
        nickname: (this.customer.customer_nickname!=null)?(this.customer.customer_nickname):this.customer.customer_name,
        gstin: (this.customer.customer_gstin).toUpperCase(),
        country_id: this.customer.customer_country,
        state_id: this.customer.customer_state,
        contact_person: this.customer.customer_contact_person,
        mobile_no: this.customer.customer_mobile,
        pan: (this.customer.customer_pan).toUpperCase(),
        address1: this.customer.customer_address_one,
        address2: this.customer.customer_address_two,
        pincode: this.customer.customer_pincode,
        city: this.customer.customer_city,
        email_id: (this.customer.customer_email).toLowerCase(),
        phone_no: (this.customer.customer_landline!=null)?(this.customer.customer_landline):this.customer.customer_mobile,
        isaction: action,
        state_name: this.customer.customer_state,
      };

      console.log(data);
      this.common.displayLoader('Please wait...');
      this.api.post('addclient', data).subscribe((res)=>{
        this.common.hideLoader();
       if((res['status']==false)&&(parseInt(res['message'])==1))
        {
          this.common.displayToaster('Sorry, Client already present in your list!');
        }
        else if(res['status']==false)
        {
          this.common.displayToaster(res['message']);
        }
        else
        {
          this.common.displayToaster(message);
          this.viewCtrl.dismiss(res);
        }
      }, (err)=>{
        console.log(err);
        this.common.displayToaster('Oops, Something went wrong!');
        this.common.hideLoader();
      });
    }
  }

  cancel(){
    let data = { role: 'cancel' };
    this.viewCtrl.dismiss(data);
  }

  getStates(){
    this.api.get('getstate').subscribe((res)=>{
      this.states = JSON.parse(atob(res['stateList']));
      console.log(this.states);
    }, (err)=>{
      console.log(err);
      this.common.displayToaster('Oops, Something went wrong!');
    });
  }

  getCountries(){
    this.api.get('getcountries').subscribe((res)=>{
      this.countries = JSON.parse(atob(res['country']));
      console.log(this.countries);
    }, (err)=>{
      console.log(err);
      this.common.displayToaster('Oops, Something went wrong!');
    });
  }
}
